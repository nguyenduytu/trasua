import { Injectable } from '@angular/core';
import { CrudBaseServiceService } from './crud-base-service.service';
import { ChiTietHoaDonModel } from '../models/chi-tiet-hoa-don.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BanHangService extends CrudBaseServiceService<ChiTietHoaDonModel> {

  constructor(http: HttpClient) {
    super({
      apiUrl: environment.apiUrl,
       entity: 'chi-tiet-hoa-don'
     }, http);
   }
}
