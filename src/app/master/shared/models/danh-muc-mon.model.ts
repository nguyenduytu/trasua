export class DanhMucMonModel {
    maMon: number;
    maLoai: number;
    tenMon: string;
    donGia: number;
    soLuong:number;
}

export enum ListMon {
    TatCa = 0,
    TraSua = 1,
    TraVangSua=2,
    TraTraiCay=3,
    TraDaXay=4,
    Topping=5
}