export class DanhMucNVLModel {
    maNVL: number;
    maNCC: number;
    maLoai: number;
    tenNVL: string;
    DVT: string;
    giaNVL: number;
    tonKho: number;
}