import { DanhMucMonModel } from './danh-muc-mon.model';

export class ChiTietHoaDonModel {
    id: number;
    soHoaDon: number;
    maMon: number;
    slMon: number;
    donGia: number;
    mon: DanhMucMonModel;
}