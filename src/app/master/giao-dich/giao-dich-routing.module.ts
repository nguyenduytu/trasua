import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HoaDonComponent } from './hoa-don/hoa-don.component';
import { NhapHangComponent } from './nhap-hang/nhap-hang.component';


const routes: Routes = [
  {path: 'hoadon', component:HoaDonComponent},
  {path: 'nhaphang', component:NhapHangComponent},
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class GiaoDichRoutingModule { }
