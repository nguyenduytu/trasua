import { Component, OnInit } from '@angular/core';
import { DanhMucMonModel } from 'src/app/master/shared/models/danh-muc-mon.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DanhMucMonService } from 'src/app/master/shared/services/danh-muc-mon.service';

@Component({
  selector: 'app-danh-muc-mon-edit',
  templateUrl: './danh-muc-mon-edit.component.html',
  styleUrls: ['./danh-muc-mon-edit.component.css']
})
export class DanhMucMonEditComponent implements OnInit {
  public mon: DanhMucMonModel
  constructor(
    public activatedRouteService: ActivatedRoute,
    public monService: DanhMucMonService,
    public routerService: Router
  ) { }

  ngOnInit(): void {
    this.mon = new DanhMucMonModel();
    this.loadData();
    this.onEdit();
    
  }
  loadData() {
    this.activatedRouteService.params.subscribe((data: Params) => {
      let maMon = data['maMon']
      this.monService.get(maMon).subscribe((mon: DanhMucMonModel) => {
        this.mon = mon;
      });
    });
  }
  
  onEdit() {
    this.monService.patch(this.mon.maMon, this.mon).subscribe(data => {
      this.mon = data;
      alert("Cập nhật thành công");
       this.routerService.navigate(['/hanghoa/danhmucmon/list'])
    })
  }
}
