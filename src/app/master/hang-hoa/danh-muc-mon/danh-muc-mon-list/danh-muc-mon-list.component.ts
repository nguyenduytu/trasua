import { Component, OnInit } from '@angular/core';
import { DanhMucMonModel } from 'src/app/master/shared/models/danh-muc-mon.model';
import { DanhMucMonService } from 'src/app/master/shared/services/danh-muc-mon.service';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { ExcelServiceService } from 'src/app/master/shared/services/excel-service.service';

@Component({
  selector: 'app-danh-muc-mon-list',
  templateUrl: './danh-muc-mon-list.component.html',
  styleUrls: ['./danh-muc-mon-list.component.css']
})
export class DanhMucMonListComponent implements OnInit {
  [x: string]: any;
  locMon='TAT_CA';
  tenMon: string;

  constructor(
    private service:DanhMucMonService ,
    private excelService: ExcelServiceService
  ) { }

  lstMon: Array<DanhMucMonModel>=[];

  ngOnInit(): void {
    this.loadMon(); 
  }
  
  loadMon(){
    this.service.getAll(RequestQueryBuilder.create({
      fields:['maMon','maLoai','tenMon','donGia'],
    })).subscribe(res => {
      this.lstMon =res;
      this.lstTemp = [...res];
      console.log(this.lstMon)
    })
  }
  onDelete(maMon:number) {
    let confirmResult = confirm("Bạn có chắc chắn muốn xóa ?");
    if (confirmResult) {
      this.service.delete(maMon).subscribe(data => {
        console.log(data);
         this.loadMon();
      })
    }
  }
  xuatfile() {
    this.excelService.exportAsExcelFile(this.lstMon, 'Danh sách Món');
  }
  getShowMon(maLoai: number){
    const dkTatCa = this.locMon === 'TAT_CA';
    const dkTraSua = this.locMon === 'TRASUA' && maLoai == 1;
    const dkTraVS = this.locMon === 'TRAVS' && maLoai == 2;
    const dkTraTraiCay = this.locMon === 'TRATRAICAY' && maLoai == 3;
    const dkTraDX = this.locMon === 'TRADAXAY' && maLoai == 4;
    const dktopping = this.locMon === 'TOPPING' && maLoai == 5;

    return dkTatCa || dkTraSua || dkTraVS || dkTraTraiCay || dkTraDX|| dktopping;
  }

    filter(event) {
      const val = event.target.value.toLowerCase();
      // filter our data
      const temp = this.lstTemp.filter(f => {
        return f.tenMon.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.lstMon = temp;
  }
}
