import { Component, OnInit } from '@angular/core';
import { DanhMucMonModel } from 'src/app/master/shared/models/danh-muc-mon.model';
import { DanhMucMonService } from 'src/app/master/shared/services/danh-muc-mon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-danh-muc-mon-add',
  templateUrl: './danh-muc-mon-add.component.html',
  styleUrls: ['./danh-muc-mon-add.component.css']
})
export class DanhMucMonAddComponent implements OnInit {
  public mon: DanhMucMonModel;

  constructor(private service:DanhMucMonService ,
    public routerService : Router
    ) { }

  ngOnInit(): void {
    this.mon = new DanhMucMonModel();
    this.onAddMon();
  }
  onAddMon(){
    this.service.create(this.mon).subscribe(res => {
      this.mon=res;
     // console.log(res) 
     alert("Thêm thành công");
      this.routerService.navigate(['/hanghoa/danhmucmon/list'])
     })
  }
}
