import { Component, OnInit } from '@angular/core';
import { DanhMucNVLModel } from 'src/app/master/shared/models/danh-muc-nvl.model';
import { DanhMucNVLServiceService } from 'src/app/master/shared/services/danh-muc-nvlservice.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-danh-muc-nvl-edit',
  templateUrl: './danh-muc-nvl-edit.component.html',
  styleUrls: ['./danh-muc-nvl-edit.component.css']
})
export class DanhMucNVLEditComponent implements OnInit {
  public nguyenlieu: DanhMucNVLModel;
  //public maNVL: number;
  constructor(
    private NVLservice: DanhMucNVLServiceService,
    public activatedRouteService: ActivatedRoute,
    public routerService: Router
  ) { }
  ngOnInit() {
    this.nguyenlieu = new DanhMucNVLModel();
    this.loadData();
    this.onEdit();
  }
  loadData() {
    this.activatedRouteService.params.subscribe((data: Params) => {
      let maNVL = data['maNVL']
      this.NVLservice.get(maNVL).subscribe((nguyenlieu: DanhMucNVLModel) => {
        this.nguyenlieu = nguyenlieu;
      });
    });
  }
  onEdit() {
    this.NVLservice.patch(this.nguyenlieu.maNVL, this.nguyenlieu).subscribe(data => {
      this.nguyenlieu = data;
      alert("Cập nhật thành công");
       this.routerService.navigate(['/hanghoa/danhmucNVL/list'])
    })
  }
}
