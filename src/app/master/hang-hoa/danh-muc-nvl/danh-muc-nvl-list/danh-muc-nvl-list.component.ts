import { Component, OnInit } from '@angular/core';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { DanhMucNVLModel } from 'src/app/master/shared/models/danh-muc-nvl.model';
import { DanhMucNVLServiceService } from 'src/app/master/shared/services/danh-muc-nvlservice.service';
import { ExcelServiceService } from 'src/app/master/shared/services/excel-service.service';

@Component({
  selector: 'app-danh-muc-nvl-list',
  templateUrl: './danh-muc-nvl-list.component.html',
  styleUrls: ['./danh-muc-nvl-list.component.css']
})
export class DanhMucNVLListComponent implements OnInit {
  [x: string]: any;
  filterStatus = 'XEM_TAT_CA';
  locNguyenLieu = 'TAT_CA';
  nguyenlieu: DanhMucNVLModel;
  tenNVL: string;  
  lstNguyenLieu: Array<DanhMucNVLModel>=[]
  

  constructor(private service: DanhMucNVLServiceService,
    private excelService: ExcelServiceService) { }

  ngOnInit() {
    this.loadNguyenLieu();
    // this.xuatFile();
    // this.Search();
  }

  loadNguyenLieu() {
    this.service.getAll(RequestQueryBuilder.create({
      fields: ['maNVL', 'maNCC', 'maLoai', 'tenNVL', 'DVT', 'giaNVL', 'tonKho'],
    })).subscribe(res => {
      this.lstNguyenLieu = res;
      this.lstTemp = [...res];
      console.log(this.lstNguyenLieu);
    });
  }
  onDelete(maNVL:number) {
    let confirmResult = confirm("Bạn có chắc chắn muốn xóa ?");
    if (confirmResult) {
      this.service.delete(maNVL).subscribe(data => {
        console.log(data);
         this.loadNguyenLieu();
      })
    }
  }

  getShowStatus(tonKho: number) {
    const dkXemTatCa = this.filterStatus === 'XEM_TAT_CA';
    const dkConHang = this.filterStatus === 'CON_HANG' && tonKho > 0;
    const dkHetHang = this.filterStatus === 'HET_HANG' && tonKho == 0;
    return dkXemTatCa || dkConHang || dkHetHang;
  }
  getShowNguyenLieu(maLoai: number) {
    const dkTatCa = this.locNguyenLieu === 'TAT_CA';
    const dkTra = this.locNguyenLieu === 'TRA' && maLoai == 1;
    const dkBot = this.locNguyenLieu === 'BOT' && maLoai == 2;
    const dkMut = this.locNguyenLieu === 'MUT' && maLoai == 3;
    const dkSyrup = this.locNguyenLieu === 'SYRUP' && maLoai == 4;
    const dkDuong = this.locNguyenLieu === 'DUONG' && maLoai == 5;
    const dktranchau = this.locNguyenLieu === 'TRANCHAU' && maLoai == 6;
    const dkSuadac = this.locNguyenLieu === 'SUADAC' && maLoai == 7;

    return dkTatCa || dkTra || dkBot || dkMut || dkSyrup || dkDuong || dktranchau || dkSuadac;
  }
  
  xuatFile() {
    this.excelService.exportAsExcelFile(this.lstNguyenLieu, 'Danh sách NVL');
  }

  filter(event) {
      const val = event.target.value.toLowerCase();
      // filter our data
      const temp = this.lstTemp.filter(f => {
        return f.tenNVL.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.lstNguyenLieu = temp;
  }
  
}
