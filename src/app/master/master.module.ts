import { NgModule,  CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './element/menu/menu.component';
import { NavbarComponent } from './element/navbar/navbar.component';
import { HangHoaComponent } from './hang-hoa/hang-hoa.component';
import { TongQuanComponent } from './tong-quan/tong-quan.component';
import { MasterRoutingModule } from './master-routing.module';
import { MasterComponent } from './master.component';
import {FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BaoCaoComponent } from './bao-cao/bao-cao.component';
import { BanHangComponent } from './ban-hang/ban-hang.component';


@NgModule({ 
  declarations: [
    MenuComponent,
    NavbarComponent,
    HangHoaComponent,
    TongQuanComponent,
    MasterComponent,
    BaoCaoComponent,
    BanHangComponent

    ],
  imports: [
    CommonModule,
    MasterRoutingModule,
    FormsModule,
    RouterModule,
   // ChartsModule
  ],
  exports: [MasterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MasterModule { }
