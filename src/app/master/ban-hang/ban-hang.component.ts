import { Component, OnInit } from '@angular/core';
import { DanhMucMonModel, ListMon } from '../shared/models/danh-muc-mon.model';
import { DanhMucMonService } from '../shared/services/danh-muc-mon.service';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { ChiTietHoaDonModel } from '../shared/models/chi-tiet-hoa-don.model';
import { BanHangService } from '../shared/services/ban-hang.service';

@Component({
  selector: 'app-ban-hang',
  templateUrl: './ban-hang.component.html',
  styleUrls: ['./ban-hang.component.css']
})
export class BanHangComponent implements OnInit {
  // ListMon = ListMon;
  // select: number;
  locMon = 'TATCA';
  lstChiTietHoaDon: Array<ChiTietHoaDonModel> = [];
  listMonDaChon: Array<DanhMucMonModel> = [];
//total=0;
  mon: DanhMucMonModel;
  slMon: number;
  maMon: DanhMucMonModel;
  lstMon: Array<DanhMucMonModel> = [];
  public chitiethoadon: ChiTietHoaDonModel;

  constructor(
    public service: DanhMucMonService,
    public banhangService: BanHangService) { }

  ngOnInit(): void {
    this.loadMon();
    this.loadChiTietHoaDon();
  }

  loadMon() {
    this.service.getAll(RequestQueryBuilder.create({
      fields: ['maMon', 'maLoai', 'tenMon', 'donGia'],
    })).subscribe(res => {
      this.lstMon = res;
    })
  }

  loadChiTietHoaDon() {
    this.banhangService.getAll({
      fields: ['soHoaDon', 'maMon', 'slMon', 'donGia'],
      join: [
        { field: 'mon', select: ['loaiMon', 'tenMon', 'donGia'] }
      ]
    }).subscribe(res => {
      this.lstChiTietHoaDon = res;
    });
  }

  phanLoaiMon(maLoai: number) {
    const dkTatCa = this.locMon === 'TATCA';
    const dkTraSua = this.locMon === 'TRASUA' && maLoai == 1;
    const dkTraVS = this.locMon === 'TRAVS' && maLoai == 2;
    const dkTraTraiCay = this.locMon === 'TRATRAICAY' && maLoai == 3;
    const dkTraDX = this.locMon === 'TRADAXAY' && maLoai == 4;
    const dktopping = this.locMon === 'TOPPING' && maLoai == 5;
    return dkTatCa || dkTraSua || dkTraVS || dkTraTraiCay || dkTraDX || dktopping;
  }

  // abc(tenMon: string) {
  //   this.listaa.push(tenMon);
  // }

  chonMon(mon: DanhMucMonModel) {
    this.listMonDaChon.push(mon);
    console.log(this.listMonDaChon);
  }

  onDeleteItem(item) {
    this.listMonDaChon.splice(item, 1)
  }

  getSLMon(item) {
    const value = item.target.value
    this.slMon = value;
  }

  getTongTien(item: DanhMucMonModel) {
    if (item) {
      const thanhTien = item.soLuong * item.donGia;
      return thanhTien;
    }
  }
  getTongHoaDon(item: DanhMucMonModel) {
    let total = 0;
   this.listMonDaChon.forEach(item => {
     total += (item.soLuong * item.donGia)
    //total += this.getTongTien(item)
   })
   return total;
  }
  
  // getTongHoaDon(item:DanhMucMonModel){
  //   let total = 0;
  //   for(let index in item){
  //     total = item.soLuong * item.donGia;
  //   }
  //     return total;
  // }
 

  ThanhToan() { }
}
