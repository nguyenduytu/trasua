import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import {HangHoaModule} from './master/hang-hoa/hang-hoa.module';
import { BaoCaoModule } from './master/bao-cao/bao-cao.module';
import { GiaoDichComponent } from './master/giao-dich/giao-dich.component';
import { HoaDonComponent } from './master/giao-dich/hoa-don/hoa-don.component';
import { NhapHangComponent } from './master/giao-dich/nhap-hang/nhap-hang.component';
//import {ChartsModule} from 'ng2-charts';

//import { BaoCaoModule } from './master/bao-cao.module';


@NgModule({
  declarations: [
    AppComponent,
    GiaoDichComponent,
    HoaDonComponent,
    NhapHangComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    HangHoaModule,
    BaoCaoModule,
    //ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
